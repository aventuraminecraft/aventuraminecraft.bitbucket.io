// Aventura Minecraft - Survival Season 2 - Overworld Map

// Map settings:
// =============
range = 9600
title = Aventura Minecraft
blurb = Season 2 Survival
showorigin = false
showcoordinates = true
x = -1130
z = 3222

// Map locations:
// ==============
// Format:
// Type, x, z, description, owner, href , iconIndex

Village, -1130, 3222, Spawn Town, ,_self_?src=locations/spawntown.txt&oceansrc=img/ocean.png&showcoordinates,39
Village, 0, 5330, America, ,_self_?src=locations/america.txt&oceansrc=img/ocean.png&showcoordinates,20

PlayerHouse, 800, -900, The Nizty Mountains, _Niz_, ,31

Dragon, -3300, 3782, End Portal
PlayerMachine, -954, 5842, Villager Breeder, Columbam1998
PlayerMachine, -1236, 5554, Ice Farm, Columbam1998
PlayerCastle, -600, 4000, Maks Base, MakzFazha
SavannahVillage, -4806, 505, Mesa